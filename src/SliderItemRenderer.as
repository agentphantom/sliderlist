package
{	
	import feathers.controls.BasicButton;
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ScrollContainer;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalLayout;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	
	public class SliderItemRenderer extends LayoutGroupListItemRenderer
	{		
		protected var _leftThreshold:int = 50;
		protected var _rightThreshold:int = 100;
		protected var _additionalThreshold:int = 5;
		
		protected var _scrollerContainer:ScrollContainer;
		protected var _leftContent:LayoutGroup;
		protected var _middleContent:LayoutGroup;
		protected var _rightContent:LayoutGroup;
		
		protected var _label1:Label;
		protected var _label2:Label;
		
		protected var _transparentButton:BasicButton;
		protected var _button1:Button;
		protected var _button2:Button;
		protected var _button3:Button;
					
		public function SliderItemRenderer()
		{
			super();
			this.minHeight = 70;
		}
		
		override protected function initialize():void
		{			
			this._scrollerContainer = new ScrollContainer();
			this._scrollerContainer.layout = new HorizontalLayout();
			this._scrollerContainer.hasElasticEdges = false;
			this._scrollerContainer.snapToPages = true;
			this._scrollerContainer.decelerationRate = 0.3;
			this.addChild(_scrollerContainer);
			
			this._leftContent = new LayoutGroup();
			this._leftContent.layout = new AnchorLayout();
			this._leftContent.backgroundSkin = new Quad(3, 3, 0x0066FF);
			this._scrollerContainer.addChild(this._leftContent);
			
			this._middleContent = new LayoutGroup();
			this._middleContent.layout = new AnchorLayout();
			this._middleContent.backgroundSkin = new Quad(3, 3, 0xFFFFFF);
			this._scrollerContainer.addChild(this._middleContent);
			
			this._rightContent = new LayoutGroup();
			this._rightContent.layout = new AnchorLayout();
			this._rightContent.backgroundSkin = new Quad(3, 3, 0xCC0000);
			this._scrollerContainer.addChild(this._rightContent);
			
			this._label1 = new Label();			
			this._label1.layoutData = new AnchorLayoutData(20, 10, NaN, 10, NaN, NaN);
			this._middleContent.addChild(this._label1);
			
			this._label2 = new Label();			
			this._label2.layoutData = new AnchorLayoutData(NaN, 10, 20, 10, NaN, NaN);
			this._middleContent.addChild(this._label2);
			
			this._transparentButton = new BasicButton();
			this._transparentButton.addEventListener(Event.TRIGGERED, function():void
			{
				owner.selectedIndex = owner.dataProvider.getItemIndex(data);
				
				_scrollerContainer.scrollToPageIndex(1, 0, 0.2);
				_scrollerContainer.snapToPages = true;
								
				if(!isSelected)
				{
					_scrollerContainer.scrollToPageIndex(1, 0, 0.3);
					_scrollerContainer.snapToPages = true;	
				}
			});
			this._transparentButton.layoutData = new AnchorLayoutData(0, 0, 0, 0, NaN, NaN);
			this._middleContent.addChild(this._transparentButton);
			
			var icon1:ImageLoader = new ImageLoader();
			icon1.width = icon1.height = 30;
			icon1.source = "assets/mail.png";
			
			this._button1 = new Button();
			this._button1.gap = 5;
			this._button1.visible = false;
			this._button1.width = 50;
			this._button1.layoutData = new AnchorLayoutData(0, 0, 0, NaN, NaN, 0);
			this._button1.defaultIcon = icon1;
			this._button1.label = "Unread";
			this._button1.iconPosition = Button.ICON_POSITION_TOP;
			this._button1.defaultSkin = new Quad(3, 3, 0x0066FF);
			this._leftContent.addChild(this._button1);
			
			var icon2:ImageLoader = new ImageLoader();
			icon2.width = icon1.height = 30;
			icon2.source = "assets/delete.png";
			
			this._button2 = new Button();
			this._button2.addEventListener(Event.TRIGGERED, disposeRenderer);
			this._button2.gap = 5;
			this._button2.visible = false;
			this._button2.width = 50;
			this._button2.layoutData = new AnchorLayoutData(0, NaN, 0, 50, NaN, 0);
			this._button2.defaultIcon = icon2;
			this._button2.label = "Delete";
			this._button2.iconPosition = Button.ICON_POSITION_TOP;
			this._button2.defaultSkin = new Quad(3, 3, 0xCC0000);
			this._rightContent.addChild(this._button2);
						
			var icon3:ImageLoader = new ImageLoader();
			icon3.width = icon1.height = 30;
			icon3.source = "assets/flag.png";
			
			this._button3 = new Button();
			this._button3.gap = 5;
			this._button3.visible = false;
			this._button3.width = 50;
			this._button3.layoutData = new AnchorLayoutData(0, NaN, 0, 0, NaN, 0);
			this._button3.defaultIcon = icon3;
			this._button3.label = "Flag";
			this._button3.iconPosition = Button.ICON_POSITION_TOP;
			this._button3.defaultSkin = new Quad(3, 3, 0xFF9900);	
			this._rightContent.addChild(this._button3);
			
			this._scrollerContainer.addEventListener(Event.SCROLL, scrollingHandler);
			this.addEventListener(EnterFrameEvent.ENTER_FRAME, checkSelectedIndex);
			this._scrollerContainer.addEventListener(FeathersEventType.BEGIN_INTERACTION, startDrag);
			this._scrollerContainer.addEventListener(FeathersEventType.END_INTERACTION, stopDrag);
		}
		
		override protected function commitData():void
		{
			if(this._data && this._owner)
			{				
				this.height = 70;
				this.width = owner.width;
				
				this._scrollerContainer.width = this.width;
				this._scrollerContainer.height = this.height;
				
				this._leftContent.width = this.width;
				this._leftContent.height = this.height;
				
				this._middleContent.width = this.width;
				this._middleContent.height = this.height;
				
				this._rightContent.width = this.width;
				this._rightContent.height = this.height;
									
				this._scrollerContainer.pageWidth = this.width;
				this._scrollerContainer.scrollToPageIndex(1, 0, 0);
				this._scrollerContainer.snapToPages = true;
								
				this._label1.text = this._data.title;
				this._label2.text = this._data.subtitle;
				
				this._button1.visible = false;
				this._button2.visible = false;
				this._button3.visible = false;
				
			} else
			{
				this._label1.text = "";
				this._label2.text = "";
			}
		}
				
		override protected function preLayout():void
		{
			this.width = this.owner.width;
			this.minHeight = 70;
			
			this._scrollerContainer.width = this.width;
			this._scrollerContainer.height = this.height;
			
			this._leftContent.width = this.width;
			this._leftContent.height = this.height;
			
			this._middleContent.width = this.width;
			this._middleContent.height = this.height;
			
			this._rightContent.width = this.width;
			this._rightContent.height = this.height;
							
		}
				
		protected function startDrag(event:Event):void
		{			
			this.owner.selectedIndex = owner.dataProvider.getItemIndex(data);
			
			//If the scrolling is touched by the user we show the buttons, this is to avoid unnecessary draw calls
			this._button1.visible = true;
			this._button2.visible = true;	
			this._button3.visible = true;	
		}		
		
		protected function scrollingHandler(event:Event):void
		{			
			//If slider has been dragged to the farright it will return to the middle of the right
			
			if(this._scrollerContainer.horizontalScrollPosition <= this._scrollerContainer.maxHorizontalScrollPosition/4)
			{
				this._scrollerContainer.horizontalScrollPosition = this._scrollerContainer.maxHorizontalScrollPosition/4;
			}
		}
		
		protected function stopDrag(event:Event):void
		{
			if(this._scrollerContainer.isScrolling)
			{
				checkScrollPosition();				
			} else {
				checkSelectedIndex();
			}
		}
		
		protected function checkScrollPosition():void
		{
			var currentPosition:int = this._scrollerContainer.horizontalScrollPosition;
			var tween:Tween;
			
			if (currentPosition <= Math.round(this._scrollerContainer.maxHorizontalScrollPosition/2)-this._additionalThreshold &&
				currentPosition >= 0)
			{
				this._scrollerContainer.snapToPages = false;
				
				tween = new Tween(this._scrollerContainer, 0.2);
				tween.animate("horizontalScrollPosition", (Math.round(this._scrollerContainer.maxHorizontalScrollPosition/2))-this._leftThreshold);
				Starling.juggler.add(tween);
			}	
			else if (currentPosition >= this._scrollerContainer.maxHorizontalScrollPosition-40) {
				disposeRenderer();
			} 
			
			else if(currentPosition >= Math.round(this._scrollerContainer.maxHorizontalScrollPosition/2)+this._additionalThreshold &&
				currentPosition <= this._scrollerContainer.maxHorizontalScrollPosition-this._additionalThreshold)
			{
				this._scrollerContainer.snapToPages = false;
				
				tween = new Tween(this._scrollerContainer, 0.2);
				tween.animate("horizontalScrollPosition", (Math.round(this._scrollerContainer.maxHorizontalScrollPosition/2))+this._rightThreshold);
				Starling.juggler.add(tween);				
			}
			
			
			else {
				this._scrollerContainer.snapToPages = true;
			}			
		}
		
		protected function disposeRenderer():void
		{
			this.removeEventListener(Event.SCROLL, scrollingHandler);
			this.removeEventListener(FeathersEventType.BEGIN_INTERACTION, startDrag);
			
			var tween:Tween = new Tween(this, 0.2);
			tween.animate("height", 0);
			tween.onComplete = function():void
			{
				Starling.juggler.remove(tween);
				_owner.dataProvider.removeItemAt(_owner.dataProvider.getItemIndex(data));			
			};
			Starling.juggler.add(tween);
		}
		
		protected function checkSelectedIndex():void
		{
			if(!this.isSelected)
			{
				//this.removeEventListener(EnterFrameEvent.ENTER_FRAME, checkSelectedIndex);
				this._scrollerContainer.scrollToPageIndex(1, 0, 0.3);
				this._scrollerContainer.snapToPages = true;	
			}
		}
		
	}
}