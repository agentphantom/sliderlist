package
{
	import feathers.controls.List;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;

	public class Main extends Sprite
	{	
		public function Main()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		protected function addedToStageHandler(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);	
			var myArray:Array = new Array();
			
			for(var i:uint = 0; i<100; i++)
			{
				myArray.push({title:"Item "+i, subtitle:"Subtitile from Item "+i});
			}
			
			var layoutForList:VerticalLayout = new VerticalLayout();
			layoutForList.hasVariableItemDimensions = true;
			
			var list:List = new List();
			list.typicalItem = {title:"Item X", subtitle:"Subtitile from Item X"};
			list.layout = layoutForList;
			list.backgroundSkin = new Quad(3, 3, 0xCCCCCC);
			list.itemRendererType = SliderItemRenderer;
			list.width = stage.stageWidth;
			list.height = stage.stageHeight;
			this.addChild(list);
			
			list.dataProvider = new ListCollection(myArray);			
		
		}
		
	}
}